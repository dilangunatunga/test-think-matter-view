<?php


namespace App\Http\Controllers\Data;
use App\Http\Controllers\Controller;
use App\Models\UserSecurityQuestions;


class DataRepo extends Controller
{


    function getSecurityQuestions(){

        $questions = UserSecurityQuestions::all();
        return response()->json([
            'data' => '',
            'questions'=> $questions,
            'status' => 'Success'
        ],200);
    }
}
