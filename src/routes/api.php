<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('profile2', function () {
    // Only authenticated users may enter...

})->middleware('auth:api');

Route::middleware('auth:api')->get('/profile', function (Request $request) {
    print 'Authenticate';
    print Auth::id();
});


Route::get('/demo', 'Test\TestController@demo');
Route::post('/login', 'Auth\LoginController@loginCognito');
Route::post('/reset_password_request', 'Auth\ResetPasswordController@resetPasswordRequest');
Route::post('/validate_security_question', 'Auth\ResetPasswordController@validateSecurityQuestion');
Route::post('/reset_password', 'Auth\ResetPasswordController@resetPassword');
Route::post('/logout', 'Auth\LoginController@logout');
Route::get('/get_security_questions', 'Data\DataRepo@getSecurityQuestions');
Route::post('/validate_reset_password', 'Auth\ResetPasswordController@validateResetPassword');


