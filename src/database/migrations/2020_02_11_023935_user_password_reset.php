<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserPasswordReset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_password_reset')) {
            Schema::create('user_password_reset', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('user_id');
                $table->char('code', 100);
                $table->char('verification_code', 100);
                $table->integer('status')->default(0);
                $table->dateTime('reset_at', 0)->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->index(['user_id']);
            });
        }

        Schema::table('user_password_reset', function (Blueprint $table) {

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_password_reset');
    }
}
